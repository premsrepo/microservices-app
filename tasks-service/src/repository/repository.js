'use strict'

const repository = (db) => {
  const collection = db.collection('tasks')

  const getAllTasks = () => {
    return new Promise((resolve, reject) => {
      const tasks = []
      const cursor = collection.find({}, {title: 1, id: 1})
      const addTasks = (task) => {
        tasks.push(task)
      }
      const sendTasks = (err) => {
        if (err) {
          reject(new Error('An error occured fetching all tasks, err:' + err))
        }
        resolve(tasks.slice())
      }
      cursor.forEach(addTasks, sendTasks)
    })
  }

  const getTaskById = (id) => {
    return new Promise((resolve, reject) => {
      const projection = { _id: 0, id: 1, title: 1}
      const sendTask = (err, task) => {
        if (err) {
          reject(new Error(`An error occured fetching a task with id: ${id}, err: ${err}`))
        }
        resolve(task)
      }
      collection.findOne({id: id}, projection, sendTask)
    })
  }

  const disconnect = () => {
    db.close()
  }

  return Object.create({
    getAllTasks,
    getTaskById,
    disconnect
  })
}

const connect = (connection) => {
  return new Promise((resolve, reject) => {
    if (!connection) {
      reject(new Error('connection db not supplied!'))
    }
    resolve(repository(connection))
  })
}

module.exports = Object.assign({}, {connect})
