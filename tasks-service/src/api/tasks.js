'use strict'
const status = require('http-status')

module.exports = (app, options) => {
  const {repo} = options

  app.get('/tasks', (req, res, next) => {
    repo.getAllTasks().then(tasks => {
      res.status(status.OK).json(tasks)
    }).catch(next)
  })

  app.get('/tasks/:id', (req, res, next) => {
    repo.getTaskById(req.params.id).then(task => {
      res.status(status.OK).json(task)
    }).catch(next)
  })
}
