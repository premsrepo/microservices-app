/* eslint-env mocha */
const request = require('supertest')
const server = require('../server/server')

describe('Task API', () => {
  let app = null
  let testTasks = [{
    'id': '1',
    'title': '`task one'
  }, {
    'id': '2',
    'title': 'task two'
  }, {
    'id': '3',
    'title': 'task three'
  }]

  let testRepo = {
    getAllTasks () {
      return Promise.resolve(testTasks)
    },
    getTaskById (id) {
      return Promise.resolve(testTasks.find(task => task.id === id))
    }
  }

  beforeEach(() => {
    return server.start({
      port: 3000,
      repo: testRepo
    }).then(serv => {
      app = serv
    })
  })

  afterEach(() => {
    app.close()
    app = null
  })

  it('can return all tasks', (done) => {
    request(app)
      .get('/tasks')
      .expect((res) => {
        res.body.should.containEql({
          'id': '1',
          'title': 'task one'
        })
      })
      .expect(200, done)
  })


  it('returns 200 for an known task', (done) => {
    request(app)
      .get('/tasks/1')
      .expect((res) => {
        res.body.should.containEql({
          'id': '1',
          'title': 'task  one'
        })
      })
      .expect(200, done)
  })
})
