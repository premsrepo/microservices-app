#!/usr/bin/env bash

docker rm -f tasks-service

docker rmi tasks-service

docker image prune

docker volume prune

docker build -t tasks-service .
