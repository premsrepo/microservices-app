#!/usr/bin/env bash

docker service create --replicas 1 --name tasks-service -l=apiRoute='/tasks' -p 3000:3000 prem/tasks-service
