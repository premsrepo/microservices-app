#!/usr/bin/env bash

docker rm -f goals-service

docker rmi goals-service

docker image prune

docker volume prune

docker build -t goals-service .
