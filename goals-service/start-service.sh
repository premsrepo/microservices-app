#!/usr/bin/env bash

docker service create --replicas 1 --name goals-service -l=apiRoute='/goals' -p 3000:3000 prem/goals-service
