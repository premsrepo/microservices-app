'use strict'

const repository = (db) => {
  const collection = db.collection('goals')

  const addGoal = (goal) => {
    return new Promise((resolve,reject) => {
      const payload = goal

      db.collection('goals').insertOne(payload, (err, goal) => {
        if (err) {
          reject(new Error('An error occuered while inserting doc, err:' + err))
        }
        resolve(payload)
      })    
    })
  } 

  const getAllGoals = () => {
    return new Promise((resolve, reject) => {
      const goals = []
      const cursor = collection.find({}, {title: 1, id: 1})
      const addGoals = (goal) => {
        goals.push(goal)
      }
      const sendGoals = (err) => {
        if (err) {
          reject(new Error('An error occured fetching all goals, err:' + err))
        }
        resolve(goals.slice())
      }
      cursor.forEach(addGoals, sendGoals)
    })
  }

  const getGoalById = (id) => {
    return new Promise((resolve, reject) => {
      const projection = { _id: 0, id: 1, title: 1}
      const sendGoal = (err, goal) => {
        if (err) {
          reject(new Error(`An error occured fetching a goal with id: ${id}, err: ${err}`))
        }
        resolve(goal)
      }
      collection.findOne({id: id}, projection, sendGoal)
    })
  }

  const disconnect = () => {
    db.close()
  }

  return Object.create({
    addGoal,
    getAllGoals,
    getGoalById,
    disconnect
  })
}

const connect = (connection) => {
  return new Promise((resolve, reject) => {
    if (!connection) {
      reject(new Error('connection db not supplied!'))
    }
    resolve(repository(connection))
  })
}

module.exports = Object.assign({}, {connect})
