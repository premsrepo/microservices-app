'use strict'
const status = require('http-status')

module.exports = (app, options) => {
  const {repo} = options

  app.get('/goals', (req, res, next) => {
    repo.getAllGoals().then(goals => {
      res.status(status.OK).json(goals)
    }).catch(next)
  }),

  app.get('/goals/:id', (req, res, next) => {
    repo.getGoalById(req.params.id).then(goal => {
      res.status(status.OK).json(goal)
    }).catch(next)
  })
}
