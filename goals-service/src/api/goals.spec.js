/* eslint-env mocha */
const request = require('supertest')
const server = require('../server/server')

describe('Goals API', () => {
  let app = null
  let testGoals = [{
    'id': '1',
    'title': '`goal one'
  }, {
    'id': '2',
    'title': 'goal two'
  }, {
    'id': '3',
    'title': 'goal three'
  }]

  let testRepo = {
    getAllGoals () {
      return Promise.resolve(testGoals)
    },
    getGoalById (id) {
      return Promise.resolve(testGoals.find(goal => goal.id === id))
    }
  }

  beforeEach(() => {
    return server.start({
      port: 3000,
      repo: testRepo
    }).then(serv => {
      app = serv
    })
  })

  afterEach(() => {
    app.close()
    app = null
  })

  it('can return all goals', (done) => {
    request(app)
      .get('/goals')
      .expect((res) => {
        res.body.should.containEql({
          'id': '1',
          'title': 'goal one'
        })
      })
      .expect(200, done)
  })


  it('returns 200 for an known goal', (done) => {
    request(app)
      .get('/goals/1')
      .expect((res) => {
        res.body.should.containEql({
          'id': '1',
          'title': 'goal'
        })
      })
      .expect(200, done)
  })
})
